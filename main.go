package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	listedFiles, err := ioutil.ReadDir("./")
	if err != nil {
		os.Exit(-1)
	}
	for _, listedFile := range listedFiles {
		fmt.Println(listedFile.Name())
	}
}
